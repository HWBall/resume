
# Resume

### *A responsive resume/cv site generator for GitLab Pages. Powered by Mustache templates engine & GitLab CI.*

## Quick Start 🚀

1. Star this repository. Just kidding, Fork It. 

2. Modify **data.json** with your information. 

3. Push your changes to gitlab. 

Now your resume is public on https://{username}.gitlab.io/resume/ 
The default example is here [https://hwball.gitlab.io/resume/](https://hwball.gitlab.io/resume/)

## Work locally 

To work locally do a **npm install** and **npm run build** to generate the html files on the **public** folder. 

## Customize your resume

To add your style just modify the style.css file in the public folder or the mustache template file to change the layout.

## License©️

Default theme designed by [Franklin Schamhart](https://dribbble.com/shots/1887983-Resume) and edited by myself.

Resume is licensed under the MIT Open Source license. For more information, see the LICENSE file in this repository.